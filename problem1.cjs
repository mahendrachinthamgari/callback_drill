const fs = require('fs');

function createAndDeleteFiles(numberOfFiles, folderName) {
    fs.mkdir(folderName, (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log('Directory created Sucessfully');
        }
        let randomNumber = Math.round(Math.random() * numberOfFiles);
        for (let index = 0; index < randomNumber; index++) {
            fs.writeFile(`${folderName}/index${index}.json`, JSON.stringify({ "name": "Json" }), (err) => {
                if (err) {
                    console.error(err);
                } else {
                    console.log('file created sucessfully');
                    fs.unlink(`${folderName}/index${index}.json`, (error) => {
                        if (error) {
                            console.error(error);
                        } else {
                            console.log('file deleted sucessfully');
                        }
                    });
                }
            });
        }
    });
}


module.exports = createAndDeleteFiles; 