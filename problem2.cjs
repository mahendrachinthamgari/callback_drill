const fs = require('fs');

function fileHandling(folderName) {
    fs.readFile(folderName, 'utf8', (error, data) => {
        if (error) {
            console.error(error);
        } else {
            let upperCaseData = data.toUpperCase();
            fs.writeFile('upper_case_data.txt', upperCaseData.toString(), (error) => {
                if (error) {
                    console.error(error);
                } else {
                    fs.writeFile('../test/filenames.txt', 'upper_case_data.txt\n'.toString(), (error) => {
                        if (error) {
                            console.error(error);
                        } else {
                            console.log('Data converted to upper case and written to new file sucessfull and name of the file were appended to filenames.txt');
                        }
                    });
                    fs.readFile('upper_case_data.txt', 'utf8', (error, data) => {
                        if (error) {
                            console.error(error);
                        } else {
                            let lowerCaseSentences = data.toLowerCase().split('. ').join('.\n');
                            fs.writeFile('lower_case_sentences.txt', lowerCaseSentences.toString(), (error) => {
                                if (error) {
                                    console.error(error);
                                } else {
                                    fs.appendFile('../test/filenames.txt', 'lower_case_sentences.txt\n'.toString(), (error) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            console.log('Read new file and done necessary operations on data and written that data to new file');
                                        }
                                    });
                                    fs.readFile('lower_case_sentences.txt', 'utf8', (error, data) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            let sortedSentences = data.split('\n').sort().slice(3);
                                            fs.writeFile('sorted_sentences.txt', sortedSentences.join('\n').toString(), (error) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    fs.appendFile('../test/filenames.txt', 'sorted_sentences.txt'.toString(), (error) => {
                                                        if (error) {
                                                            console.error(error);
                                                        } else {
                                                            console.log('Read the new file and done necessary operations and stored data to new file and appended name of the file to filenames.txt');
                                                        }
                                                    });
                                                    fs.readFile('../test/filenames.txt', 'utf8', (error, data) => {
                                                        if (error) {
                                                            console.error(error);
                                                        } else {
                                                            const filenames = data.split('\n');
                                                            for (let index = 0; index < filenames.length; index++) {
                                                                fs.unlink(`../test/${filenames[index]}`, (error) => {
                                                                    if (error) {
                                                                        console.error(error);
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}


module.exports = fileHandling;